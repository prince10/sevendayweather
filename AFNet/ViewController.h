//
//  ViewController.h
//  AFNet
//
//  Created by Prince on 19/11/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>


@end

