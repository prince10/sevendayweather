//
//  ViewController.m
//  AFNet
//
//  Created by Prince on 19/11/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSString *BaseURLString;
    int i;
    int j;
    NSMutableArray *discription;
    NSMutableArray *clouds;
    NSMutableArray* rain;
    NSMutableArray *humidity;
    NSMutableArray *mainArray;
    
    
    
    NSDictionary *wheatherDict;

    
}
@property (strong, nonatomic) IBOutlet UITableView *wheatherTable;


@end

@implementation ViewController
@synthesize wheatherTable;
- (void)viewDidLoad {
    
    clouds=[[NSMutableArray alloc]init];
    discription=[[NSMutableArray alloc]init];
    humidity=[[NSMutableArray alloc]init];
    mainArray=[[NSMutableArray alloc]init];
    [self getapi];
    
    
  
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


-(void) getapi
{

AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
[manager GET:[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast/daily?q=London&mode=json&units=metric&cnt=7&appid=2de143494c0b295cca9337e1e96b00e0"]
  parameters:nil
     success:^(AFHTTPRequestOperation *operation, id json ) {
         if (json) {
             //NSLog(@"%@",json);
             
             
             NSDictionary *cityDict=json[@"city"];
            // NSLog(@"%@",cityDict);
             
             NSDictionary *coordDict=cityDict[@"coord"];
            // NSLog(@"%@",coordDict);
             
             
             NSArray *listArray=json[@"list"];
             for (i=0; i<listArray.count; i++) {
                 NSLog(@"%@",listArray);
             
             NSDictionary *listDict=listArray[i];
           //  NSLog(@"%@",listDict);
             
             NSNumber *cloudsDict=listDict[@"clouds"];
             NSLog(@"clouds     %@",cloudsDict);
             
                 [clouds addObject:cloudsDict.stringValue];
                 
             

             
             
             NSNumber *humidityDict=listDict[@"humidity"];
             NSLog(@"humidity   %@",humidityDict);
                 
                 [humidity addObject:humidityDict.stringValue];
                 
             
             NSNumber *pressureDict=listDict[@"pressure"];
             NSLog(@"pressure %@",pressureDict);
             
             NSNumber *rainDict=listDict[@"rain"];
             NSLog(@"rain   %@",rainDict);
             
             NSNumber *speedDict=listDict[@"speed"];
             NSLog(@"speed  %@",speedDict);

             
             NSArray *wheatherArray=listDict[@"weather"];
                 for (j=0; j<wheatherArray.count; j++) {
                     // NSLog(@"%@",wheatherArray);
                     
                     NSDictionary *wheatherDict=wheatherArray[j];
                     // NSLog(@"%@",wheatherDict);
                     
                     
                     NSString *descString=wheatherDict[@"description"];
                     NSLog(@"%@",descString);
                     
                     [discription addObject:descString];
                     
                     NSString *mainString=wheatherDict[@"main"];
                     NSLog(@"%@",mainString);
                     [mainArray addObject:mainString];
                     
             }
             
                 
                
              
                 
          
                 
                 
                 
                 
                 
             }
             [wheatherTable reloadData];
             
         }
     }
     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error.description);
         NSLog(@"ERROR: %@ \n\n 1. ",error.localizedDescription);
         
         
         
     }];
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView

{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return clouds.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"identy"];
    UILabel *label1 = (UILabel *)[cell viewWithTag:100];
    label1.text=clouds[indexPath.row];
    
     UILabel *label2 = (UILabel *)[cell viewWithTag:101];
    label2.text=discription[indexPath.row];
    
    UILabel *label3 = (UILabel *)[cell viewWithTag:102];
    label3.text=humidity[indexPath.row];
    
    
    UILabel *label4 = (UILabel *)[cell viewWithTag:103];
    label4.text=mainArray[indexPath.row];
    
    
    return cell;
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
